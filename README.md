# RabbitMQ TLS Server

Proof of concept on how to boot RabbitMQ in Docker with TLS.

It will auto-create a new CA, and sign it's server and client certificates via it.

The client cert and key will be outputted upon boot as well as the CACert bundle.

## Building

Simply run `./.ci/build.sh` locally to build a local version of the docker image.

## Running

The easiest is to run:

```bash
docker run --rm --it --name rabbitmq-tls -p local-port:5671 rabbitmq-tls:local
```

Or if you want to mount a volume for the client certificate:

_Linux_
```bash
docker run --rm --it --name rabbitmq-tls -v /path/to/local:/home/client -p local-port:5671 rabbitmq-tls:local
```

_Windows_
```powershell
docker run --rm --it --name rabbitmq-tls -v c:/path/to/local:/home/client -p local-port:5671 rabbitmq-tls:local
```

## Testing

### Inside container

```bash
docker exec --it rabbitmq-tls /bin/bash
cd /home/client && openssl s_client -connect localhost:5671 -cert cert.pem -key key.pem -CAfile /home/secure-bunnies/cacert.pem
```

### Outside container

```bash
openssl s_client -connect localhost:local-port -cert /path/to/local/cert.pem -key /path/to/local/key.pem -CAfile /path/to/local/cacert.pem
```
