FROM rabbitmq:3.7.8-alpine

RUN apk --no-cache update \
  && apk add bash openssl \
  && cd /home \
  && mkdir -p secure-bunnies server client \
  && cd secure-bunnies \
  && mkdir -p certs private scripts \
  && chmod 700 private \
  && echo 01 > serial \
  && touch index.txt

COPY config/rabbitmq.config /etc/rabbitmq/rabbitmq.config
COPY config/openssl.cnf /home/secure-bunnies/openssl.cnf
COPY scripts/*.sh /home/secure-bunnies/scripts/

RUN chmod +x /home/secure-bunnies/scripts/*.sh

RUN /home/secure-bunnies/scripts/prepare-server.sh
# RUN rabbitmqctl stop_app

CMD /home/secure-bunnies/scripts/generate-client-key.sh && rabbitmq-server