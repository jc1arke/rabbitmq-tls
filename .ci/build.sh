#!/usr/bin/env bash
IMAGE_TAG=${CI_PIPELINE_IID:-local}
docker build --no-cache -t rabbitmq-tls:${IMAGE_TAG} .
