#!/usr/bin/env bash
set -eu

cd /home/secure-bunnies

echo "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-="
echo "Switch context: CA"

echo "Generating self-signed certificate..."
openssl req -x509 -config openssl.cnf -newkey rsa:2048 -days 365 -out cacert.pem -outform PEM -subj /CN=SecureBunnies/ -nodes

echo "Encoding certification with DER..."
openssl x509 -in cacert.pem -out cacert.cer -outform DER

echo "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-="
echo "Switch context: SERVER"
cd /home/server

echo "Generating private key..."
openssl genrsa -out key.pem 2048

echo "Generating certificate from private key..."
openssl req -new -key key.pem -out req.pem -outform PEM -subj /CN=$(hostname)/O=server/ -nodes

echo "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-="
echo "Switch context: CA"
cd /home/secure-bunnies

echo "Signing server certificate with CA..."
openssl ca -config openssl.cnf -in /home/server/req.pem -out /home/server/cert.pem -notext -batch -extensions server_ca_extensions

echo "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-="
echo "Switch context: SERVER"
cd /home/server

echo "Generate key store to contain certificate..."
openssl pkcs12 -export -out keycert.p12 -in cert.pem -inkey key.pem -passout pass:roboconf

echo "Preperation complete"
