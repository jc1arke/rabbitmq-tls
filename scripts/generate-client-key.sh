#!/usr/bin/env bash
set -eu

echo "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-="
echo "Switch context: CLIENT"
cd /home/client

echo "Generating private key..."
openssl genrsa -out key.pem 2048

echo "Generating certificate..."
openssl req -new -key key.pem -out req.pem -outform PEM -subj /CN=$(hostname)/O=client/ -nodes

echo "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-="
echo "Switch context: CA"
cd /home/secure-bunnies

echo "Signing client certificate..."
openssl ca -config openssl.cnf -in /home/client/req.pem -out /home/client/cert.pem -notext -batch -extensions client_ca_extensions

echo "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-="
echo "Switch context: CLIENT"
cd /home/client

echo "Generating key store to contain certificate..."
openssl pkcs12 -export -out key-store.p12 -in cert.pem -inkey key.pem -passout pass:roboconf

echo "Generating key store to contain the certificate of the CA..."
openssl pkcs12 -export -out trust-store.p12 -in /home/secure-bunnies/cacert.pem -inkey /home/secure-bunnies/private/cakey.pem -passout pass:roboconf

echo "Client key generation complete"

echo "Client details:"
echo "---------------------- cert.pem ----------------------"
cat cert.pem

echo "---------------------- key.pem ----------------------"
cat key.pem

echo "---------------------- cacert.pem ----------------------"
cat /home/secure-bunnies/cacert.pem